import * as q from "./q";
const Q = q.Q;
//// 1.2 write a query
import {crimeData} from "../test/data";
const rawData = crimeData.data;


export const theftsQuery = new q.FilterNode(((x)=> (x[2]).match(/THEFT/)));
export const autoTheftsQuery = new q.FilterNode(((x)=> (x[3] as string) === "MOTOR VEHICLE THEFT"));

//// 1.4 clean the data

export const cleanupQuery = new q.ApplyNode(((x)=>({description: x[2], category: x[3], area: x[4], date: x[5]})));

//// 1.6 reimplement queries with call-chaining

export const cleanupQuery2 = new q.ApplyNode(((x)=>({description: x[2], category: x[3], area: x[4], date: x[5]})));
export const theftsQuery2 = new q.FilterNode(((x)=> (((x).description).match(/THEFT/))));
export const autoTheftsQuery2 = new q.FilterNode(((x)=> (x.category) === "MOTOR VEHICLE THEFT"));



//// 4 put your queries here (remember to export them for use in tests)
//1
export const crimesMiddayLakeCity = new q.ThenNode(new q.FilterNode((x) => (x[4] as string) === "LAKECITY"), new q.FilterNode((x) => (x[6] as number) >= 1000 && (x[6] as number) <= 1400));
//returns the crimes that occur in Lake City that occur between 10am and 2pm. 

//2 
export const rapesSouthPrecinctCount = new q.ThenNode(new q.ThenNode(new q.FilterNode((x) => (x[2].match(/RAPE/))), new q.FilterNode((x) => ((x[7] as string) === "SOUTH"))), new q.CountNode());
// returns the number of rapes that occur in the south precinct 

//3
export const crimesDUI = new q.ThenNode(new q.FilterNode((x) => (x[2].match(/DUI/))), new q.FilterNode((x) => (x[4] as string).startsWith("C")));
//returns the dui crimes that occur in neighborhoods with names that start with C. 

//4
export const burglaryOrRobbery = new q.ThenNode(new q.FilterNode((x) => (x[2].match(/ROBBERY/) || x[2].match(/BURGLARY/))), new q.ApplyNode((x)=>(x[9]-x[6])));
// returns time between when a crime occurs and is reported for crimes that are either listed as a robbery or a burglary

//5
export const movieRatings = new q.JoinNode((left, right) => (left[0] === right[0]), new q.ApplyNode((x) => (x.movieData)), new q.ApplyNode((x) => (x.movieGenreData)));
//joins movies based on their id to see information about what genres are associated with what movies. 