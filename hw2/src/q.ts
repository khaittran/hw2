/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 */

// This class represents all AST Nodes.

export class ASTNode {
    public readonly type: string;

    predicate: (datum: any) => boolean;
    public static callback = (x) => {
        let left = x.left;
        let right = x.right;
        let target = {};
        Object.assign(target, left);
        Object.assign(target, right);
        //Object.assign
        return target;
    }
    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
       
        throw new Error("Execute not implemented for " + this.type + " node.");
    }

    optimize(): ASTNode {
        //console.log(this);
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 implement call-chaining
    filter(predicate: (datum: any) => boolean): ASTNode {
        
        //this.predicate = predicate;
        return new ThenNode(this, new FilterNode(predicate));
    }

    apply(callback: (datum: any) => any): ASTNode {
       
        return new ThenNode(this, new ApplyNode(callback));
    }

    count(): ASTNode {
        
        return new ThenNode(this, new CountNode());
    }

    product(query: ASTNode): ASTNode {
        return new CartesianProductNode(this, query);
        //throw new Error("Call chaining not implemented.");
    }

    join(query: ASTNode, relation: string | ((left: any, right: any) => any)): ASTNode {
        if(typeof(relation) === 'string') {
            //return new CartesianProductNode(new ThenNode(this, new FilterNode((x)=>(x[relation]))));
            let filterCallback = (x) => {
                return x.left.hasOwnProperty(relation) && x.right.hasOwnProperty(relation) && x.left[relation] == x.right[relation];
            }
            (filterCallback as any).relation = relation;
            //console.log("type of relation");
            //console.log(typeof(relation))
            
            let applyCallback = (x) => {
                let target = {};
                Object.assign(target, x.left);
                Object.assign(target, x.right);
                return target;
            }
            return new ThenNode(new ThenNode(new CartesianProductNode(this, query), new FilterNode(filterCallback)), new ApplyNode(applyCallback));
           // throw new Error("hack - field joins not supported yet");
        } else {
            let filterCallback = (x) => {
                return relation(x.left, x.right) === true;
            }
            (filterCallback as any).relation = relation;
            let callback = (x) => {
                let left = x.left;
                let right = x.right;
                let target = {};
                Object.assign(target, left);
                Object.assign(target, right);
                //Object.assign
                return target;
            }
            //this.execute(this.data);
            //need some way to combine together-my thought is using a then node and filter node just trying to figure out how to do it. 
            return new ThenNode(new ThenNode(new CartesianProductNode(this, query), new FilterNode(filterCallback)), 
                new ApplyNode(callback));

           // return new CartesianProductNode( new FilterNode(relation), new FilterNode(relation));
            //new CartesianProductNode(this, query);
       }
    }
}

// The Id node just outputs all records from input.
export class IdNode extends ASTNode {
    constructor() {
        super("Id");
    }

    //// 1.1 implement execute
    execute(data: any[]): any{
        return data;
    }

}

// We can use an Id node as a convenient starting point for
// the call-chaining interface.
export const Q = new IdNode();

// The Filter node uses a callback to throw out some records.
export class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    //// 1.1 implement execute
    execute(data: any[]): any{
        const good = [];
        for (const datum of data) {
            if (this.predicate(datum)) {
                good.push(datum);
            }
        }
        // return array after filter with predicate
        return good;
    }
    
}
// The Then node chains multiple actions on one data structure.
export class ThenNode extends ASTNode {
    // can be any object that can run execute
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;      
    }
   
    //// 1.1 implement execute
    execute(data: any[]): any{
       // Q.data = this.first.execute(data);
        return this.second.execute(this.first.execute(data));
    }

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }
}

//// 1.3 implement Apply and Count Nodes

export class ApplyNode extends ASTNode {
    callback: (datum: any) => any;
    constructor(callback: (datum: any) => any) {
        super("Apply");
        this.callback = callback;      
    }

    execute(data: any[]): any{
        //console.log("data")
        //console.log(data)
        const out = [];
        for (const x of data) {
            //console.log("x")
            //console.log(x)
            //console.log("callback x")
            //console.log(x)
            out.push(this.callback(x));
        }
        //console.log("out after apply");
        //console.log(out);
        return out;
    }
}

export class CountNode extends ASTNode {
    constructor() {
        super("Count");
    }
    execute(data: any[]):any{
        const good = [];
        good[0] = 0;
        for (const datum of data) {
            good[0]++;
        }
        return good;
    }
}

//// 2.1 optimize queries

// This function permanently adds a new optimization function to a node type.
// An optimization function takes in a node and either returns a new,
// optimized node or null if no optimizations can be performed. AddOptimization
// will register this function to a particular node type, so that it will be called
// as part of that node's optimize() method, along with all other registered
// optimizations.
function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    const oldOptimize = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function(this: ASTNode): ASTNode {
        const newThis = oldOptimize.call(this);
        return opt.call(newThis) || newThis;
    };
}

// // // For example, the following small optimization removes unnecessary Id nodes.
// AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
//     if (this.first instanceof IdNode) {
//         return this.second;
//     } else if (this.second instanceof IdNode) {
//         return this.first;
//     } else {
//         return null;
//     }
// });

// The above optimization has a few notable side effects. First, it removes the
// root IdNode from your call chain, so if you were depending on that to exist
// your other optimizations may be confused. Also, it returns the interesting
// subtree directly, without trying to optimize it more. It is up to you to
// determine where additional optimization should occur.

// We won't specifically test for this optimization, so if it's giving you
// a headache feel free to comment it out.

// You can add optimizations for part 2.1 here (or anywhere below).
// ...
function remove_id() {
   
    if (this.first.first instanceof IdNode)
        return new ThenNode(this.first.second, this.second);
}

function recount(){
    if(this.first instanceof ThenNode 
        && this.first.second instanceof FilterNode 
        && this.second instanceof CountNode){
        return new ThenNode(this.first.first, new CountIfNode(this.first.second.predicate));
    }
    if( this.second instanceof CountNode && this.first instanceof FilterNode){
        return new CountIfNode(this.first.predicate);
    }
}

function mergFilter(){
    if(this.first instanceof FilterNode 
        && this.second instanceof FilterNode){
        return new FilterNode((x)=>(this.first.predicate(x) && (this.second.predicate(x))));
    }
    if(this.second instanceof FilterNode 
        && this.first instanceof ThenNode 
        && this.first.second instanceof FilterNode){
        return new ThenNode(this.first.first, new FilterNode((x)=> (this.first.second.predicate(x) && (this.second.predicate(x)))));
    }
}

//return new ThenNode(new ThenNode(new CartesianProductNode(this, query), new FilterNode((x)=>( relation(x.left, x.right) == true))), 
  //              new ApplyNode(callback));

  //hash join should look like 
  // return new ThenNode(new ThenNode(new CartesianProductNode(this, query), new FilterNode(filterCallback)), new ApplyNode(applyCallback));
function joinOptimization() {
    if(this.first instanceof ThenNode && 
        this.first.first instanceof CartesianProductNode && 
        this.first.second instanceof FilterNode && this.first.second.predicate.relation instanceof Function && this.second instanceof ApplyNode ) {
        return new JoinNode(this.first.second.predicate.relation, this.first.first.left, this.first.first.right)
    }
}

//hash join should look like 
  // return new ThenNode(new ThenNode(new CartesianProductNode(this, query), new FilterNode(filterCallback)), new ApplyNode(applyCallback));
function hashJoinOptimization() {
    if(this.first instanceof ThenNode && 
        this.first.first instanceof CartesianProductNode && this.first.second instanceof FilterNode 
        && typeof(this.first.second.predicate.relation) === 'string' 
         && this.second instanceof ApplyNode ) {
        return new HashJoinNode(this.first.second.predicate.relation, this.first.first.left, this.first.first.right)
    }
}

AddOptimization(ThenNode, mergFilter);
AddOptimization(ThenNode, remove_id);
AddOptimization(ThenNode, recount);
AddOptimization(ThenNode, joinOptimization);
AddOptimization(ThenNode, hashJoinOptimization);


//// 2.2 internal node types and CountIf

export class CountIfNode extends ASTNode {
    predicate: (datum: any) => boolean;
    constructor(predicate: (datum: any) => boolean) {
        super("CountIf");
        this.predicate = predicate;
        
    }
    execute(data: any[]):any{

        return new CountNode().execute(new FilterNode(this.predicate).execute(data));
    }
}

//// 3.1 cartesian products

export class CartesianProductNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    
    constructor(left: ASTNode, right: ASTNode) {
        super("CartesianProduct");
        this.left = left;
        this.right= right;
    }
    
    execute(data: any[]): any{
        const out = [];
        for(const dum of this.left.execute(data)){
            for(const dumn of this.right.execute(data)){
                out.push({left: dum, right: dumn});
            }
        }
        return out;
    }
}

//// 3.2-3.6 joins and hash joins

/* Hint: Recall from the spec that a join of two arrays P and Q by a
   function f(l, r) is the array with one entry for each pair of a record
   in P and Q for which f returns true and that each entry should have all
   fields and values that either record in the pair had.

   Most notably, if both records had the same field, use the value from the
   record from Q. In JavaScript, this can be achieved with Object.assign:
   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
*/

export class JoinNode extends ASTNode {
    left: ASTNode
    right: ASTNode //value that we take if field values are there. 
    relation: ((left: any, right: any) => any)

    constructor(relation: ((left: any, right: any) => any), left: ASTNode, right: ASTNode) { // you may want to add some proper arguments to this constructor
        super("Join");
        this.left = left;
        this.right = right;
        this.relation = relation;
    }

    execute(data: any[]): any{
        //console.log("In execute of join");
        //console.log("data");
        //console.log(data);
        let out = [];

        for(const leftData of this.left.execute(data)){
            for(const rightData of this.right.execute(data)){
                //console.log("left data")
                //console.log(leftData);
                //console.log("right data");
                //console.log(rightData);
                //console.log("function result");
                //console.log(this.relation(leftData, rightData));
               if(this.relation(leftData, rightData) === true) {
                  let target = {};
                  Object.assign(target, leftData);
                  Object.assign(target, rightData);
                  //const returnedTarget = Object.assign(leftData, rightData);
                  out.push(target);
                  //console.log("out");
                  //console.log(out);
               }
            }
        }
        //console.log("final out");
        //console.log(out);
        //console.log("data afterwards");
        //console.log(data);
        return out;
    }
}

/* Hint: While optimizing the fluent join to use your internal JoinNode,
   if you find yourself needing to compare two functions for equality,
   you can do so with the Javascript operator `===` (pointer equality).
 */

export class HashJoinNode extends ASTNode {
    field: string; 
    left: ASTNode;
    right: ASTNode; 
    constructor(field: string, left: ASTNode, right:ASTNode) { // you may want to add some proper arguments to this constructor
        super("HashJoin");
        this.field = field; 
        this.left = left;
        this.right = right;
        //throw new Error("Unimplemented AST node " + this.type);
    }

    execute(data: any[]): any{
        const leftRecordMatch = new Map();
        for(const leftData of this.left.execute(data)) {
            if(leftData.hasOwnProperty(this.field)) {
                const currentFieldValue = leftData[this.field];
                if(leftRecordMatch.has(currentFieldValue)) {
                    const currentSet = leftRecordMatch.get(currentFieldValue);
                    currentSet.push(leftData);
                    leftRecordMatch.set(currentFieldValue, currentSet);
                } else {
                    const setToAdd = []; 
                    setToAdd.push(leftData);
                    leftRecordMatch.set(currentFieldValue, setToAdd);
                }
            }
            
        }
        const rightRecordMatch = new Map(); 
        for(const rightData of this.right.execute(data)) {
            if(rightData.hasOwnProperty(this.field)) {
                const currentFieldValue = rightData[this.field];
                if(rightRecordMatch.has(currentFieldValue)) {
                    const currentSet = rightRecordMatch.get(currentFieldValue);
                    currentSet.push(rightData);
                    rightRecordMatch.set(currentFieldValue, currentSet);
                } else {
                    const setToAdd = []; 
                    setToAdd.push(rightData);
                    rightRecordMatch.set(currentFieldValue, setToAdd);
                }
            }
        }
        //console.log("left record match");
        //console.log(leftRecordMatch);
        //console.log("right record match");
        //console.log(rightRecordMatch);

        const out = [];
        for(const key of leftRecordMatch.keys()){
            const leftSet = leftRecordMatch.get(key);
            let rightSet = null;
            if(rightRecordMatch.has(key)) {
                rightSet = rightRecordMatch.get(key);
            }
            if(rightSet !== null) {
                for(let leftRecord of leftSet) {
                    for(let rightRecord of rightSet) {
                        let target = {};
                        Object.assign(target, leftRecord);
                        Object.assign(target, rightRecord);
                        out.push(target);
                        //out.push(leftData rightData});
                    } 
                }
            }
        }
        return out;
        //need to form the tables 


        //then you can loop through those tables 
    }
}
