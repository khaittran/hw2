import {expect} from "chai";

import * as q from "../src/q";
const Q = q.Q;
import * as queries from "../src/queries";

import {crimeData} from "./data";
const rawData = crimeData.data;

function flattenThenNodes(query) {
    if (query instanceof q.ThenNode) {
        return flattenThenNodes(query.first).concat(flattenThenNodes(query.second));
    } else if (query instanceof q.IdNode) {
        return [];
    } else {
        return [query];
    }
}

function expectQuerySequence(query, seq) {
    const flat = flattenThenNodes(query);
    //console.log("flat");
    //console.log(flat);
    //console.log("seq");
    //console.log(seq);
    expect(flat.length).to.be.equal(seq.length);
    for (let i = 0; i < flat.length; i++) {
        expect(flat[i].type).to.be.equal(seq[i]);
    }
}

function cleanData() {
    const out = [];
    for (const x of rawData) {
        out.push({description: x[2], category: x[3], area: x[4], date: x[5]});
    }
    return out;
}

// You should provide at least 5 tests, using your own queries from Part 4
// and either the crime data from test/data.ts or your own data. You can look
// at test/q.ts to see how to import and use the queries and data.
// The outline of the first test has been provided as an example - you should
// modify it so it actually does something interesting.

describe("custom", () => {

    it("Hash Join no matching field names", () => {
        const query = new q.HashJoinNode("a", new q.IdNode(), new q.FilterNode((x)=> { return x.hasOwnProperty['a'] }));
        const datum = {a: 3, b: 2};
        const datum1 = {b: 4, d: 5}
        const data = [datum, datum1];

        console.log(query.execute(data));
        expect(query.execute(data)).to.have.length(0);
        expect(query.optimize().execute(data)).to.have.length(0);
        //const query = Q;
    });

    it("Test Cartesian products With long arrays", () => {
        const testData = ["a", "b", "c","d","e","f","g","k","o","n","m","t","y","z","x","c","v","b","n","q"];
        const testExpected = [];
        for(const dum of testData){
            for(const dumn of testData){
                testExpected.push({left: dum, right: dumn});
            }
        }

        const query = new q.CartesianProductNode(new q.IdNode(), new q.IdNode());
        const out = query.execute(testData);
        const len = testData.length;
        expect(out).to.have.length(len * len);
        expect(out).to.deep.equal(testExpected);
    }); 

    it("Test hash Join matching field names and value", () => {
        const query = new q.HashJoinNode("a", new q.IdNode(), new q.FilterNode((x)=> x.a == 3));
        const datum = {a: 3, b: 2};
        const datum1 = {b: 4, d: 5}
        const data = [datum, datum1];


        expect(query.execute(data)).to.deep.equal([data[0]]);
        expect(query.optimize().execute(data)).to.have.length(data.length-1);
        
    });

    it("Test hash joins with crimeData", () => {
        const query = new q.HashJoinNode("description", new q.IdNode(), new q.FilterNode((x)=> (((x).description).match(/THEFT/)))).count();
        const out = query.execute(cleanData());
        let good = 0;
        for (const datum1 of rawData) {
            if(datum1[2].toString().match(/THEFT/)){
                    good++;
            }
        }
        expect(out).to.be.an("Array");
        expect(out).to.have.length(1);
        expect(out[0]).to.not.equal(good);
    }); 

    it("Test Joins with crimeData", () => {
        const query = Q.join(Q, (l, r) => l[0] === r[0]).apply((x) => x[0]).count();
        const out = query.execute(rawData);
        const expected = rawData.map((x) => x[0]);
        expect(out[0]).to.deep.equal(expected.length);
    }); 
});
